Alea
=====================================
### World of Warcraft Guild Website
Alea is a responsive, AngularJS based theme for World of Warcraft guild websites.

  - Bootstrap 3
  - AngularJS
  - Fully styled in SCSS

> This is a work in progress. Lots of stuff needs to be done.

Preview
----
[![General components](http://imgur.com/lx2eUnxs.jpg "Alea v0.0.1")](http://i.imgur.com/lx2eUnx.jpg)
[![Raid Icons](http://imgur.com/v8HvDPDs.jpg "Alea v0.0.1")](http://i.imgur.com/v8HvDPD.jpg)
[![Raid progress](http://i.imgur.com/KUnPg0Ps.jpg "Alea v0.0.1")](http://i.imgur.com/KUnPg0P.jpg)

Version
----
0.0.1

Installation
--------------

```sh
git clone https://bitbucket.org/packadic/frontend-theme-alea
bower install
grunt build
```

##### Serve
```sh
grunt watch
```

License
----
Free to use, alter and publish for non-commercial purposes.
