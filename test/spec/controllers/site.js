'use strict';

describe('Controller: SitectrlCtrl', function () {

  // load the controller's module
  beforeEach(module('aleaApp'));

  var SitectrlCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    SitectrlCtrl = $controller('SitectrlCtrl', {
      $scope: scope
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(scope.awesomeThings.length).toBe(3);
  });
});
