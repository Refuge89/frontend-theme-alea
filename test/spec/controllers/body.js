'use strict';

describe('Controller: BodyctrlCtrl', function () {

  // load the controller's module
  beforeEach(module('aleaApp'));

  var BodyctrlCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    BodyctrlCtrl = $controller('BodyctrlCtrl', {
      $scope: scope
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(scope.awesomeThings.length).toBe(3);
  });
});
