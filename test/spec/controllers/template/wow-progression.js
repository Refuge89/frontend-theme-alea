'use strict';

describe('Controller: TemplateWowProgressionCtrl', function () {

  // load the controller's module
  beforeEach(module('aleaApp'));

  var TemplateWowProgressionCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    TemplateWowProgressionCtrl = $controller('TemplateWowProgressionCtrl', {
      $scope: scope
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(scope.awesomeThings.length).toBe(3);
  });
});
