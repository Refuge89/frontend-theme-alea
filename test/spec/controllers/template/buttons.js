'use strict';

describe('Controller: TemplateButtonsCtrl', function () {

  // load the controller's module
  beforeEach(module('aleaApp'));

  var TemplateButtonsCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    TemplateButtonsCtrl = $controller('TemplateButtonsCtrl', {
      $scope: scope
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(scope.awesomeThings.length).toBe(3);
  });
});
