'use strict';

describe('Controller: TemplateGeneralCtrl', function () {

  // load the controller's module
  beforeEach(module('aleaApp'));

  var TemplateGeneralCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    TemplateGeneralCtrl = $controller('TemplateGeneralCtrl', {
      $scope: scope
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(scope.awesomeThings.length).toBe(3);
  });
});
