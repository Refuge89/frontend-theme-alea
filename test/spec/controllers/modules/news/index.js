'use strict';

describe('Controller: ModulesNewsIndexCtrl', function () {

  // load the controller's module
  beforeEach(module('aleaApp'));

  var ModulesNewsIndexCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    ModulesNewsIndexCtrl = $controller('ModulesNewsIndexCtrl', {
      $scope: scope
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(scope.awesomeThings.length).toBe(3);
  });
});
