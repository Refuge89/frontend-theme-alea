'use strict';

describe('Controller: TemplatesTypoCtrl', function () {

  // load the controller's module
  beforeEach(module('aleaApp'));

  var TemplatesTypoCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    TemplatesTypoCtrl = $controller('TemplatesTypoCtrl', {
      $scope: scope
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(scope.awesomeThings.length).toBe(3);
  });
});
