'use strict';

describe('Controller: TemplatesTemplatenavCtrl', function () {

  // load the controller's module
  beforeEach(module('aleaApp'));

  var TemplatesTemplatenavCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    TemplatesTemplatenavCtrl = $controller('TemplatesTemplatenavCtrl', {
      $scope: scope
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(scope.awesomeThings.length).toBe(3);
  });
});
