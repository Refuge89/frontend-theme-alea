'use strict';

describe('Controller: TemplatesNavCtrl', function () {

  // load the controller's module
  beforeEach(module('aleaApp'));

  var TemplatesNavCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    TemplatesNavCtrl = $controller('TemplatesNavCtrl', {
      $scope: scope
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(scope.awesomeThings.length).toBe(3);
  });
});
