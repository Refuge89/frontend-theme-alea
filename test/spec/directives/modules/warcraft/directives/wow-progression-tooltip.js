'use strict';

describe('Directive: modules/warcraft/directives/wowProgressionTooltip', function () {

  // load the directive's module
  beforeEach(module('aleaApp'));

  var element,
    scope;

  beforeEach(inject(function ($rootScope) {
    scope = $rootScope.$new();
  }));

  it('should make hidden element visible', inject(function ($compile) {
    element = angular.element('<modules/warcraft/directives/wow-progression-tooltip></modules/warcraft/directives/wow-progression-tooltip>');
    element = $compile(element)(scope);
    expect(element.text()).toBe('this is the modules/warcraft/directives/wowProgressionTooltip directive');
  }));
});
