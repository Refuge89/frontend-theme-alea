'use strict';

/**
 * @ngdoc overview
 * @name aleaApp
 * @description
 * # aleaApp
 *
 * Main module of the application.
 */
angular
    .module('aleaApp', [
        'ngAnimate',
        'ngCookies',
        'ngResource',
        'ngRoute',
        'ngSanitize',
        'ngTouch',
        'ui.bootstrap',
        'warcraft',
        'hljs'
    ])
    .config(function ($routeProvider, $locationProvider, hljsServiceProvider) {
        hljsServiceProvider.setOptions({
            // replace tab with 4 spaces
            tabReplace: '    '
        });
        //$locationProvider.html5Mode(true).hashPrefix("#");

        $routeProvider
            /* -   -   -   -   -   - /
             Template features
             /* -   -   -   -   -   - */
            .when('/', {
                templateUrl: 'views/main.html',
                controller: 'MainCtrl'
            })
            .when('/template/general', {
                templateUrl: 'views/template/general.html',
                controller: 'TemplateGeneralCtrl'
            })
            .when('/template/typo', {
                templateUrl: 'views/template/typo.html',
                controller: 'TemplateTypoCtrl'
            })
            .when('/template/buttons', {
                templateUrl: 'views/template/buttons.html',
                controller: 'TemplateButtonsCtrl'
            })
            .when('/template/modals', {
                templateUrl: 'views/template/modals.html',
                controller: 'TemplateModalsCtrl'
            })
            .when('/template/portlets', {
                templateUrl: 'views/template/portlets.html',
                controller: 'TemplatePortletsCtrl'
            })
            .when('/template/utilities', {
                templateUrl: 'views/template/utilities.html',
                controller: 'TemplateUtilitiesCtrl'
            })

            .when('/template/wow-raid-icons', {
                templateUrl: 'views/template/wow-raid-icons.html',
                controller: 'TemplateWowRaidIconsCtrl'
            })
            .when('/template/wow-api', {
                templateUrl: 'views/template/wow-api.html',
                controller: 'TemplateWowApiCtrl'
            })


            /* -   -   -   -   -   - /
             Pages
             /* -   -   -   -   -   - */
            .when('/frontpage', {
                templateUrl: 'views/frontpage.html',
                controller: 'FrontpageCtrl'
            })



            .otherwise({
                redirectTo: '/'
            });
    });
