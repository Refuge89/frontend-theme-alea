"use strict";

angular.module('warcraft')

    .factory('bnetApi', ['$http', 'warcraftService', 'warcraftCache', function ($http, warcraftService, warcraftCache) {

        var request = function (uri, success, disableCache) {
            var url = 'http://' + warcraftService.config.regions[warcraftService.config.region.toLowerCase()].host + '/api/wow/' + uri;
            var cache = warcraftCache.get(url);

            if (cache == undefined || disableCache == true) {
                var delim = (url.indexOf("?") > -1 ? "&" : "?");

                return $http.jsonp(url + delim +'jsonp=JSON_CALLBACK')
                    .success(function (data, status) {
                        if (success != undefined && status == 200) {
                            warcraftCache.put(url, data);
                            return success(data);
                        }
                    })
                    .error(function (data, status) {
                        // @todo this
                    });
            }
            else {
                return success(cache);
            }
        };

        var data = function () {
            var container = {
                battlegroups: {},
                character: {
                    races: {},
                    classes: {},
                    achievements: {}
                },
                guild: {
                    rewards: {},
                    perks: {},
                    achievements: {}
                },
                item: {
                    classes: {}
                },
                talents: {},
                pet: {
                    types: {

                    }
                }
            };

            _.each(container, function (obj, id) {
                //console.log(obj, _.size(obj), id);
                if (_.size(obj) > 0) {
                    _.each(obj, function (childObj, childId) {
                        request('data/' + id + '/' + childId, function (response) {
                            container[id][childId] = response[childId];
                        });
                    });
                }
                else {
                    request('data/' + id + (id == 'battlegroups' ? '/' : ''), function (response) {
                        container[id] = (id == 'battlegroups' ? response.battlegroups : response);
                    });
                }
            });

            return container;
        }();

        var transform = function () {
            var transformer = function (object, from, to, value) {
                var result;
                _.each(object, function (obj, i) {
                    if (obj[from] === value) {
                        result = obj[to];
                    }
                });
                return result;
            };
            var classes = function (from, to, value) {
                return transformer(data.character.classes, from, to, value);
            };
            var races = function (from, to, value) {
                return transformer(data.character.races, from, to, value);
            };
            var genders = function (from, to, value) {
                var genders = [
                    { id: 0, name: 'male' },
                    { id: 1, name: 'female' }
                ]
                return transformer(genders, from, to, value);
            };
            var itemClasses = function (from, to, value) {
                return transformer(data.item.classes, from, to, value);
            };

            return {
                gender: genders,
                itemClass: itemClasses,
                class: classes,
                race: races
            }
        }();

        return {
            data: data,
            request: request,
            transform: transform
        }
    }]);