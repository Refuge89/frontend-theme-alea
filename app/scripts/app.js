'use strict';

var restServer = {
    url: 'http://platform/api/v1/',
    apiKey: ''
};


/**
 * @ngdoc overview
 * @name aleaApp
 * @description
 * # aleaApp
 *
 * Main module of the application.
 */
angular
    .module('aleaApp', [
        'ngAnimate',
        'ngCookies',
        'ngResource',
        'ngRoute',
        'ngSanitize',
        'ngTouch',
        'ui.bootstrap',
        'warcraft',
        'hljs'
    ])
    .config(function ($routeProvider, $locationProvider, hljsServiceProvider, warcraftServiceProvider, $httpProvider) {
        $httpProvider.defaults.headers.common["Api-Request"] = "true";
        $httpProvider.defaults.headers.common["Api-Key"] = "Xocg0fB7699JkghrM9ppUKilxp7PJBTt";

        hljsServiceProvider.setOptions({
            // replace tab with 4 spaces
            tabReplace: '    '
        });
        warcraftServiceProvider.config({
            region: 'eu',
            locale: 'en_GB',
            realm: 'Hellfire',
            guild: 'Alea iacta est'
        });
        //$locationProvider.html5Mode(true).hashPrefix("#");

        $routeProvider
            /* -   -   -   -   -   - /
             Template features
             /* -   -   -   -   -   - */
            .when('/', {
                templateUrl: 'views/main.html',
                controller: 'MainCtrl'
            })
            .when('/template/general', {
                templateUrl: 'views/template/general.html',
                controller: 'TemplateGeneralCtrl'
            })
            .when('/template/typo', {
                templateUrl: 'views/template/typo.html',
                controller: 'TemplateTypoCtrl'
            })
            .when('/template/buttons', {
                templateUrl: 'views/template/buttons.html',
                controller: 'TemplateButtonsCtrl'
            })
            .when('/template/modals', {
                templateUrl: 'views/template/modals.html',
                controller: 'TemplateModalsCtrl'
            })
            .when('/template/portlets', {
                templateUrl: 'views/template/portlets.html',
                controller: 'TemplatePortletsCtrl'
            })
            .when('/template/utilities', {
                templateUrl: 'views/template/utilities.html',
                controller: 'TemplateUtilitiesCtrl'
            })

            .when('/template/wow-raid-icons', {
                templateUrl: 'views/template/wow-raid-icons.html',
                controller: 'TemplateWowRaidIconsCtrl'
            })
            .when('/template/wow-api', {
                templateUrl: 'views/template/wow-api.html',
                controller: 'TemplateWowApiCtrl'
            })
            .when('/template/wow-progression', {
                templateUrl: 'views/template/wow-progression.html',
                controller: 'TemplateWowProgressionCtrl'
            })
            .when('/template/darktip', {
                templateUrl: 'views/template/darktip.html',
                controller: 'TemplateDarktipCtrl'
            })


            /* -   -   -   -   -   - /
             Pages
             /* -   -   -   -   -   - */
            .when('/frontpage', {
                templateUrl: 'views/frontpage.html',
                controller: 'FrontpageCtrl'
            })


            .otherwise({
                redirectTo: '/'
            });
    });
