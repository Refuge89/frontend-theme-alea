'use strict';

/**
 * @ngdoc function
 * @name aleaApp.controller:ModulesNewsViewCtrl
 * @description
 * # ModulesNewsViewCtrl
 * Controller of the aleaApp
 */
angular.module('aleaApp')
  .controller('ModulesNewsViewCtrl', function ($scope) {
    $scope.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];
  });
