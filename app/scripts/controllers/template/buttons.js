'use strict';

/**
 * @ngdoc function
 * @name aleaApp.controller:TemplateButtonsCtrl
 * @description
 * # TemplateButtonsCtrl
 * Controller of the aleaApp
 */
angular.module('aleaApp')
  .controller('TemplateButtonsCtrl', function ($scope) {

        // BUTTONS
        $scope.singleModel = 1;

        $scope.radioModel = 'Middle';

        $scope.checkModel = {
            left: false,
            middle: true,
            right: false
        };
  });
