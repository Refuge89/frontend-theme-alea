'use strict';

/**
 * @ngdoc function
 * @name aleaApp.controller:TemplateDarktipCtrl
 * @description
 * # TemplateDarktipCtrl
 * Controller of the aleaApp
 */
angular.module('aleaApp')
  .controller('TemplateDarktipCtrl', function ($scope) {
    $scope.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];
  });
