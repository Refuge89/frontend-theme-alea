'use strict';

/**
 * @ngdoc function
 * @name aleaApp.controller:TemplateUtilitiesCtrl
 * @description
 * # TemplateUtilitiesCtrl
 * Controller of the aleaApp
 */
angular.module('aleaApp')
  .controller('TemplateUtilitiesCtrl', function ($scope) {
    $scope.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];
  });
