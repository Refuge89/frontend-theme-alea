'use strict';

/**
 * @ngdoc function
 * @name aleaApp.controller:HeadctrlCtrl
 * @description
 * # HeadctrlCtrl
 * Controller of the aleaApp
 */
angular.module('aleaApp')
  .controller('HeadCtrl', function ($scope) {
    $scope.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];
  });
