'use strict';

/**
 * @ngdoc function
 * @name aleaApp.controller:NavCtrl
 * @description
 * # NavCtrl
 * Controller of the aleaApp
 */
angular.module('aleaApp')
    .controller('NavCtrl', function ($scope, Main) {
        $scope.awesomeThings = [
            'HTML5 Boilerplate',
            'AngularJS',
            'Karma'
        ];



    });
