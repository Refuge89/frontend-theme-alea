'use strict';

/**
 * @ngdoc function
 * @name aleaApp.controller:BodyctrlCtrl
 * @description
 * # BodyctrlCtrl
 * Controller of the aleaApp
 */
angular.module('aleaApp')
  .controller('BodyCtrl', function ($scope, $http, $rootScope, Main) {
        $scope.includes = {
            nav: 'views/nav.html',
            templateNav: 'views/template.nav.html'
        };

        $rootScope.main = Main.get(function(main){ $rootScope.main = main; });


    })
    .animation('.view-animation', function() {
        return {
            leave : function(element, done) {
                setTimeout(function(){
                    jQuery('body').css({ 'overflow': 'auto' });
                }, 800);
                done();
            },
            enter : function(element, done) {
                jQuery('body').css({ 'overflow': 'hidden' });
                done();
            }
        };
    });